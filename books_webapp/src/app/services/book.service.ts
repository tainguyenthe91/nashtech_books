import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { BookInfoModel } from '../models/bookInfo';
import { restEndPoints } from "../../environments/environment";

@Injectable({ providedIn: 'root' })
export class BookService {
  constructor(private http: HttpClient) { }

  getBooks(): Observable<BookInfoModel[]> {
    return this.http.get<BookInfoModel[]>(restEndPoints.GET_LIST_BOOK)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  createBook(book: BookInfoModel) {
    return this.http.post(restEndPoints.CREATE_BOOK, book)
    .pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}