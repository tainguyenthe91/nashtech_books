export class BookInfoModel {

	constructor(
		public title: string,
		public category: string,
		public description: string,
		public created_at: string
	) { }
}