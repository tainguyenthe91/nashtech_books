import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { BookService } from '../services/book.service';
import { BookInfoModel } from '../models/bookInfo';
import * as moment from 'moment';

@Component({
	selector: 'book',
	templateUrl: './book.component.html',
	styleUrls: ['./book.component.css']
})

export class BookComponent implements OnInit {
	categories: any = ["drama", "comedy", "sport"];
	books: BookInfoModel[] = [];
	bookForm: FormGroup;
	showModal: boolean;
	close: number = 0;
	submitted: boolean = false;

	constructor(private bookService: BookService, private formBuilder: FormBuilder) { }

	ngOnInit() {
		this.loadAllBooks();
		this.bookForm = this.createFormGroup();
		console.log(`Before: ${this.close}`);
	}

	createFormGroup() {
		return this.formBuilder.group({
			title: new FormControl('', [Validators.required, Validators.maxLength(30)]),
			category: new FormControl('', [Validators.required]),
			description: new FormControl('', [Validators.required])
		});
	}

	onSubmit() {
		this.submitted = true;
		this.bookForm.value['created_at'] = moment().unix();
		this.bookService.createBook(this.bookForm.value)
			.subscribe(() => {
				this.bookForm.reset();
				this.loadAllBooks();
				this.showDialog();
			});
	}

	loadAllBooks() {
		this.bookService.getBooks()
			.subscribe(books => {
				if (books['status'] == 200) {
					this.books = books['data'];
				} else {
					return;
				}
			});
	}

	showDialog() {
		this.showModal = true;
	}

	handleVisibleChange(closed: boolean) {
		closed ? '' : this.close++;
		console.log(`After: ${this.close}`);
	}
};