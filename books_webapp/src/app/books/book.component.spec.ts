import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from '../app.component';
import { BookComponent } from './book.component';
import { DialogComponent } from '../dialog/dialog.component';

describe('BookComponent', () => {
	let comp: BookComponent;
	let fixture: ComponentFixture<BookComponent>;
	let de: DebugElement;
	let el: HTMLElement;

	beforeEach(async(() => {
		const routes: Routes = [{
			path: '',
			component: BookComponent
		}];

		// Import dependencies to start the test module
		TestBed.configureTestingModule({
			declarations: [
				AppComponent,
				BookComponent,
				DialogComponent
			],
			imports: [
				BrowserModule,
				FormsModule,
				ReactiveFormsModule,
				HttpClientModule,
				RouterModule.forRoot(routes),
				RouterTestingModule
			],
			providers: [],
		}).compileComponents().then(() => {
			fixture = TestBed.createComponent(BookComponent);
			comp = fixture.componentInstance;	 // BookComponent test instance
			de = fixture.debugElement.query(By.css('#bookForm'));
			el = de.nativeElement;
		})
	}))

	it(`Should set submitted to true`, async(() => {
		comp.onSubmit();
		expect(comp.submitted).toBeTruthy();
	}))

	it(`Should call the onSubmit method`, async(() => {
		fixture.detectChanges();
		spyOn(comp, 'onSubmit');
		el = fixture.debugElement.query(By.css('button')).nativeElement;
		el.click();
		expect(comp.onSubmit).toHaveBeenCalledTimes(0);
	}))

	it(`Form should be invalid`, async(() => {
		comp.bookForm.controls['title'].setValue('');
		comp.bookForm.controls['category'].setValue('');
		comp.bookForm.controls['description'].setValue('');
		expect(comp.bookForm.valid).toBeFalsy();
	}))

	it(`Form should be invalid`, async(() => {
		comp.bookForm.controls['title'].setValue('Tiểu thuyết mùa đông');
		comp.bookForm.controls['category'].setValue('drama');
		comp.bookForm.controls['description'].setValue('Ánh trăng đã xuống dưới hiên nhà');
		expect(comp.bookForm.valid).toBeTruthy();
	}))
})