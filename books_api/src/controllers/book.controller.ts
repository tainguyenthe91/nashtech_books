import { Request, Response } from 'express';
import { BookServices } from '../services/book.services';

export class BookController {
  public bookServices: BookServices = new BookServices();

  public bookList = async (req: Request, res: Response) => {
    try {
      const listBook = await this.bookServices.getBooks();
      return res.status(200).json({ status: 200, data: listBook, message: 'Succesfully List Book' });
    } catch (e) {
      return res.status(400).json({ status: 400, message: e.message });
    }
  };

  public bookCreate = async (req: Request, res: Response) => {
    try {
      const book = await this.bookServices.createBook(req.body);
      return res.status(200).json({ status: 200, data: book, message: 'Succesfully Create Book' });
    } catch (e) {
      return res.status(400).json({ status: 400, message: e.message });
    }
  };

  public bookDetails = async (req: Request, res: Response) => {
    try {
      const book = await this.bookServices.findBook(req.params.id);
      return res.status(200).json({ status: 200, data: book, message: 'Succesfully Get Book' });
    } catch (e) {
      return res.status(400).json({ status: 400, message: e.message });
    }
  };

  public bookUpdate = async (req: Request, res: Response) => {
    try {
      const book = await this.bookServices.findAndUpdateBook(req.params.id, req.body);
      return res.status(200).json({ status: 200, data: book, message: 'Succesfully Update Book' });
    } catch (e) {
      return res.status(400).json({ status: 400, message: e.message });
    }
  };

  public bookDelete = async (req: Request, res: Response) => {
    try {
      const book = await this.bookServices.findAndRemoveBook(req.params.id);
      return res.status(200).json({ status: 200, data: book, message: 'Succesfully Delete Book' });
    } catch (e) {
      return res.status(400).json({ status: 400, message: e.message });
    }
  };
}
