import * as express from 'express';
const router = express.Router();

import { BookController } from '../controllers/book.controller';

const bookServices: BookController = new BookController();

router
  .get('/list', bookServices.bookList)
  .post('/create', bookServices.bookCreate)
  .get('/:bookId', bookServices.bookDetails)
  .put('/:bookId/update', bookServices.bookUpdate)
  .delete('/:bookId/delete', bookServices.bookDelete);

module.exports = router;
