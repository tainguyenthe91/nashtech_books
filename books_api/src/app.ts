import * as bodyParser from 'body-parser';
// import cors from 'cors';
const cors = require('cors');
import * as dotenv from 'dotenv';
import * as express from 'express';
import * as expressValidator from 'express-validator';
import * as mongoose from 'mongoose';
import * as path from 'path';

const indexRouter = require('./routes/index.route');
const bookRouter = require('./routes/book.route');
import { MONGODB_URI } from './util/secrets';

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: '.env' });

// Create Express servern
const app = express();
app.use(cors());

// Connect to MongoDB
const mongoUrl = MONGODB_URI;
mongoose
  .connect(mongoUrl, { useNewUrlParser: true, useFindAndModify: false })
  .then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
  })
  .catch((err) => {
    // process.exit();
  });

// Express configuration
app.set('port', process.env.PORT || 3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());

app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

/**
 * Primary app routes.
 */
app.use('/api', indexRouter);
app.use('/api/books', bookRouter);

export default app;
