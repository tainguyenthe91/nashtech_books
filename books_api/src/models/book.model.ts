import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const BookSchema = new Schema({
  title: {
    type: String,
    required: true,
    min: 10,
  },
  category: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  created_at: {
    type: String,
    required: true,
  },
});

// Sets the createdAt parameter equal to the current time
// BookSchema.pre('save', (next) => {
//   const now = new Date();
//   if (!this.created_at) {
//     this.created_at = now;
//   }
//   next();
// });

module.exports = mongoose.model('books', BookSchema);
