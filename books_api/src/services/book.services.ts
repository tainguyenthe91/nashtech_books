const Book = require('../models/book.model');

// import Validator from 'fastest-validator';
const Validator = require('fastest-validator');
const instanceValidator = new Validator();

export class BookServices {
  public getBooks = async () => {
    try {
      const books = await Book.find({})
        .sort({ created_at: 'desc' })
        .exec();
      return books;
    } catch (e) {
      throw Error(e);
    }
  };

  public createBook = async (data: any) => {
    if (!(this.validatorInputData(data) === true)) {
      throw { message: 'Validation Error' };
    }
    try {
      const model = new Book({
        title: data.title,
        category: data.category,
        description: data.description,
        created_at: data.created_at,
      });
      const book = await model.save();
      return book;
    } catch (e) {
      throw Error(e);
    }
  };

  public findBook = async (id: number) => {
    try {
      const book = await Book.findById(id);
      return book;
    } catch (e) {
      throw Error(e);
    }
  };

  public findAndUpdateBook = async (id: number, data: any) => {
    if (!(this.validatorInputData(data) === true)) {
      throw { message: 'Validation Error' };
    }

    try {
      const model = await Book.findByIdAndUpdate(id, { $set: data });
      if (model) {
        const book = await this.findBook(model.id);
        return book;
      }
      return model;
    } catch (e) {
      throw Error(e);
    }
  };

  public findAndRemoveBook = async (id: number) => {
    try {
      const model = await Book.findByIdAndDelete(id);
      return model;
    } catch (e) {
      throw Error(e);
    }
  };

  private validatorInputData = (data: any) => {
    const rulePattern = /([A-Za-z\-\’])*/;

    const bookValidate = {
      title: { type: 'string', min: 5, max: 50, pattern: rulePattern },
      category: { type: 'string', min: 5, max: 30, pattern: rulePattern },
      description: { type: 'string', min: 10, max: 150, pattern: rulePattern },
    };
    return instanceValidator.validate(data, bookValidate);
  };
}
