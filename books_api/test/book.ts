import 'mocha';
import app from "../src/app";

//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const mongoose = require('mongoose');
const Books = require('../src/models/book.model');

//Require the dev-dependencies
const chai = require('chai'), should = chai.should(), expect = chai.expect;
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

//tell mongoose to use es6 implementation of promises
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/local');
mongoose.connection
  .once('open', () => console.log('Connected!'))
  .on('error', (error) => {
    console.warn('Error : ', error);
  });


describe('Books', () => {
  beforeEach((done) => {
    // mongoose.connection.collections.books.drop(() => {
    //   done();
    // });
    // Books.remove({}, (err) => {
    // done();
    // });
    done();
  });

  // Check hello work
  describe("Getting Starter", () => {
    it('It should get a message is Success', (done) => {
      chai.request(app).get('/api/').end((err, res, body) => {
        should.exist(res);
        res.should.be.an('object')
        res.should.have.status(200);
        done();
      });
    })
  })

  /*
   * Test the /POST route
   */
  describe("Creating books", () => {
    it("it should create a book and return this book", (done) => {
      let book = {
        title: 'Microservice 1',
        category: 'drama',
        description: 'Design Thinking Architech 1',
        created_at: '1561631193',
      };
      chai.request(app).post('/api/books/create/').send(book).end((err, res, body) => {
        should.exist(res);
        res.should.have.status(200);
        res.body.should.have.property('data');
        done();
      });
    });
  });

  /*
   * Test to GET books
   */
  describe('Listing books', () => {
    it("it should get all book record", (done) => {
      chai.request(app).get('/api/books/list')
        .end((err, res) => {
          should.exist(res);
          res.should.have.status(200);
          res.body.should.have.property('data');
          done();
        });
    });
  });

  // Test to get single book record
  describe('Get a book', () => {
    it("should get a single book record", (done) => {
      const id = `5d19d9cdd4f9461bde79d29d`;
      chai.request(app).get(`/api/books/${id}`).end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('data');
          done();
        });
    });
  });
});