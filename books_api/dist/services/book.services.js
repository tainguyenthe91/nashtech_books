"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const book_model_1 = require("../models/book.model");
// import Validator from 'fastest-validator';
const Validator = require('fastest-validator');
const instanceValidator = new Validator();
class BookServices {
    constructor() {
        this.getBooks = () => __awaiter(this, void 0, void 0, function* () {
            try {
                const books = yield book_model_1.Book.find({})
                    .sort({ created_at: 'desc' })
                    .exec();
                return books;
            }
            catch (e) {
                throw Error(e);
            }
        });
        this.createBook = (data) => __awaiter(this, void 0, void 0, function* () {
            if (!(this.validatorInputData(data) === true)) {
                throw { message: 'Validation Error' };
            }
            try {
                const model = new book_model_1.Book({
                    title: data.title,
                    category: data.category,
                    description: data.description,
                    created_at: data.created_at,
                });
                const book = yield model.save();
                return book;
            }
            catch (e) {
                throw Error(e);
            }
        });
        this.findBook = (id) => __awaiter(this, void 0, void 0, function* () {
            try {
                const book = yield book_model_1.Book.findById(id);
                return book;
            }
            catch (e) {
                throw Error(e);
            }
        });
        this.findAndUpdateBook = (id, data) => __awaiter(this, void 0, void 0, function* () {
            if (!(this.validatorInputData(data) === true)) {
                throw { message: 'Validation Error' };
            }
            try {
                const model = yield book_model_1.Book.findByIdAndUpdate(id, { $set: data });
                if (model) {
                    const book = yield this.findBook(model.id);
                    return book;
                }
                return model;
            }
            catch (e) {
                throw Error(e);
            }
        });
        this.findAndRemoveBook = (id) => __awaiter(this, void 0, void 0, function* () {
            try {
                const model = yield book_model_1.Book.findByIdAndDelete(id);
                return model;
            }
            catch (e) {
                throw Error(e);
            }
        });
        this.validatorInputData = (data) => {
            const rulePattern = /([A-Za-z\-\’])*/;
            const bookValidate = {
                title: { type: 'string', min: 5, max: 50, pattern: rulePattern },
                category: { type: 'string', min: 5, max: 30, pattern: rulePattern },
                description: { type: 'string', min: 10, max: 150, pattern: rulePattern },
            };
            return instanceValidator.validate(data, bookValidate);
        };
    }
}
exports.BookServices = BookServices;
//# sourceMappingURL=book.services.js.map