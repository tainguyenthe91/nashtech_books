"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const book_services_1 = require("../services/book.services");
class BookController {
    constructor() {
        this.bookServices = new book_services_1.BookServices();
        this.bookList = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const listBook = yield this.bookServices.getBooks();
                return res.status(200).json({ status: 200, data: listBook, message: 'Succesfully List Book' });
            }
            catch (e) {
                return res.status(400).json({ status: 400, message: e.message });
            }
        });
        this.bookCreate = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const book = yield this.bookServices.createBook(req.body);
                return res.status(200).json({ status: 200, data: book, message: 'Succesfully Create Book' });
            }
            catch (e) {
                return res.status(400).json({ status: 400, message: e.message });
            }
        });
        this.bookDetails = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const book = yield this.bookServices.findBook(req.params.id);
                return res.status(200).json({ status: 200, data: book, message: 'Succesfully Get Book' });
            }
            catch (e) {
                return res.status(400).json({ status: 400, message: e.message });
            }
        });
        this.bookUpdate = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const book = yield this.bookServices.findAndUpdateBook(req.params.id, req.body);
                return res.status(200).json({ status: 200, data: book, message: 'Succesfully Update Book' });
            }
            catch (e) {
                return res.status(400).json({ status: 400, message: e.message });
            }
        });
        this.bookDelete = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const book = yield this.bookServices.findAndRemoveBook(req.params.id);
                return res.status(200).json({ status: 200, data: book, message: 'Succesfully Delete Book' });
            }
            catch (e) {
                return res.status(400).json({ status: 400, message: e.message });
            }
        });
    }
}
exports.BookController = BookController;
//# sourceMappingURL=book.controller.js.map