"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errorHandler = require("errorhandler");
const app_1 = require("./app");
/**
 * Error Handler. Provides full stack - remove for production
 */
app_1.default.use(errorHandler());
/**
 * Start Express server.
 */
app_1.default.listen(app_1.default.get('port'));
//# sourceMappingURL=server.js.map