"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const router = express.Router();
const book_controller_1 = require("../controllers/book.controller");
const bookServices = new book_controller_1.BookController();
router
    .get('/list', bookServices.bookList)
    .post('/create', bookServices.bookCreate)
    .get('/:bookId', bookServices.bookDetails)
    .put('/:bookId/update', bookServices.bookUpdate)
    .delete('/:bookId/delete', bookServices.bookDelete);
module.exports = router;
//# sourceMappingURL=book.route.js.map