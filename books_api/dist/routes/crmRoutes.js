"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crmController_1 = require("../controllers/crmController");
class Routers {
    constructor() {
        this.contactController = new crmController_1.ContactController();
    }
    routers(app) {
        app.get('/contact', this.contactController.getContacts)
            .post('/contact', this.contactController.addNewContact);
        app.get('/contact/:contactId', this.contactController.getContactWithID)
            .put('/contact/:contactId/update', this.contactController.updateContact)
            .delete('/contact/:contactId/delete', this.contactController.deleteContact);
    }
}
exports.Routers = Routers;
//# sourceMappingURL=crmRoutes.js.map