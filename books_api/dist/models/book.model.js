"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const BookSchema = new Schema({
    title: {
        type: String,
        required: true,
        min: 10,
    },
    category: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    created_at: {
        type: String,
        required: true,
    },
});
// Sets the createdAt parameter equal to the current time
BookSchema.pre('save', next => {
    let now = new Date();
    if (!this.created_at) {
        this.created_at = now;
    }
    next();
});
exports.Book = mongoose.model('books', BookSchema);
module.exports = exports.Book;
//# sourceMappingURL=book.model.js.map