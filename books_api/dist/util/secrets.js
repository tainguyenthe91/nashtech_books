"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
const fs = require("fs");
if (fs.existsSync('.env')) {
    console.log('Using .env file to supply config environment variables');
    dotenv.config({ path: '.env' });
}
else {
    console.log('Using .env.example file to supply config environment variables');
    dotenv.config({ path: '.env.example' }); // you can delete this after you create your own .env file!
}
exports.ENVIRONMENT = process.env.NODE_ENV;
const prod = exports.ENVIRONMENT === 'production'; // Anything else is treated as 'dev'
exports.MONGODB_URI = prod ? process.env.MONGODB_URI : process.env.MONGODB_URI_LOCAL;
if (!exports.MONGODB_URI) {
    console.log('No mongo connection string. Set MONGODB_URI environment variable.');
    process.exit(1);
}
//# sourceMappingURL=secrets.js.map