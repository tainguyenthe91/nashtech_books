"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
// import cors from 'cors';
const cors = require('cors');
const dotenv = require("dotenv");
const express = require("express");
const expressValidator = require("express-validator");
const mongoose = require("mongoose");
const path = require("path");
const indexRouter = require('./routes/index.route');
const bookRouter = require('./routes/book.route');
const secrets_1 = require("./util/secrets");
// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: '.env' });
// Create Express servern
const app = express();
app.use(cors());
// Connect to MongoDB
const mongoUrl = secrets_1.MONGODB_URI;
mongoose
    .connect(mongoUrl, { useNewUrlParser: true, useFindAndModify: false })
    .then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
})
    .catch((err) => {
    // process.exit();
});
// Express configuration
app.set('port', process.env.PORT || 3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(express.static(path.join(__dirname, 'public')));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
/**
 * Primary app routes.
 */
app.use('/api', indexRouter);
app.use('/api/books', bookRouter);
exports.default = app;
//# sourceMappingURL=app.js.map