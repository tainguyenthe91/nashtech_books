## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:3000/api/`. If you get a response from API with the text is `Success` that is the connection success

## Development watch typescript

Run `npm run watch-ts` to watching everything in the typescript code

## Format

Run `npm run format` to execute the script with Prettierc

## Lint

Run `npm run lint` to execute the script with TSLint

## Build

Run `npm run prod` to build and start the project. The build artifacts will be stored in the `dist/` directory. Use `npm run build` for build production.

## Running unit tests

Run `npm run test` to execute the unit tests with Mocha